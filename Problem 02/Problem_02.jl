### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 49a97c78-8b58-4654-8aa8-a9779a7deaa2
using Markdown,Pkg,Images,ImageView,TestImages,FileIO,PlutoUI,Flux,Statistics, onecold, crossentropy, throttle, logitcrossentropy,InteractiveUtils, partition, Plots,BSON,Printf

# ╔═╡ 16d408e2-f184-403f-adc0-ca60591ec17a
Pkg.activate("Project.toml")

# ╔═╡ db8c5276-e81e-4e6a-8ce6-f0325f0405fd
using Base.Iterators: repeated

# ╔═╡ f856e09c-d9ff-4781-9e7e-315cba7a013a
using Flux: gradient

# ╔═╡ 32a74df5-19e6-4946-9e21-ee7141b640a1
using Flux: Data.DataLoader

# ╔═╡ 2c4952bf-8af5-40ec-a0fe-dde8ecee7216
using Flux: @epochs

# ╔═╡ a656c9a2-4072-459e-be9c-d20a519fad0a
using Parameters: @with_kw

# ╔═╡ ea4c8e60-b428-11ec-3423-81a7dd55b8b8
md"# ARI711S Individual Assignment: Problem 02 
* Refer to the ReadMe.md for the question and expected solutions "

# ╔═╡ 14338b42-65b3-4f65-a7f6-9b929a29bd94
Flux: onehotbatch

# ╔═╡ 7af02932-45bd-4021-acc8-4b097dea8f4f
function load_images(path::String)
    img_paths = readdir(path)
    imgs = []
    for img_path in img_paths
        push!(imgs, load(string(path,img_path)))
    end
    return imgs
end

# ╔═╡ 550173b7-47d8-4a2c-8474-222f7c1e0323
test_1imgs = [load(img) for img in readdir("Data/problem 02/test/normal", join = true)]

# ╔═╡ 3dd6294e-2f50-45cf-bfa6-0d06c0cece91
test_2imgs = [load(img) for img in readdir("Data/problem 02/test/pneumonia", join = true)]

# ╔═╡ ede21768-43f2-4346-be4a-9b2bfa499177
train_1imgs = [load(img) for img in readdir("Data/problem 02/train/normal", join = true)]

# ╔═╡ 7efd48e1-338a-464c-9a2c-1018de7c37ef
tn_img = load("Data/problem 02/train/normal/IM-0115-0001.jpeg")

# ╔═╡ 97ab55f2-3555-46e5-97e2-f63562c26374
train_2imgs = [load(img) for img in readdir("Data/problem 02/train/pneumonia", join = true)]

# ╔═╡ 99824380-2798-4073-bd21-f21187af8a01
tp_img = load("Data/problem 02/train/pneumonia/person1_bacteria_1.jpeg")

# ╔═╡ b4f0f8d2-2f42-4199-a57f-a69219825428
val_1imgs = [load(img) for img in readdir("Data/problem 02/val/normal", join = true)]

# ╔═╡ 6e4087b1-df9a-474f-913b-9b2f44342fc9
val_2imgs = [load(img) for img in readdir("Data/problem 02/val/pneumonia", join = true)]

# ╔═╡ 2cc81e1a-cd01-4d6b-8a5d-72f382b28c29
size(test_1imgs)

# ╔═╡ 3f605167-fa4b-45eb-a3cd-e9b8f322dcf0
size(test_2imgs)

# ╔═╡ f1087c03-7dda-402a-a509-c821001c8b21
size(train_1imgs)

# ╔═╡ a7e86e21-4e6d-4ee8-9af5-e392eb827312
size(train_2imgs)

# ╔═╡ f45d729d-5361-403a-b134-e7b646de4722
size(val_1imgs)

# ╔═╡ 38a8168f-06bd-4b93-a151-078eb81358e0
size(val_2imgs)

# ╔═╡ acc4158d-7df6-42fb-a930-8537eb2ac0bb
df(x) = gradient(f, x)[1]

# ╔═╡ 907a88aa-adf9-487a-b3e4-2e5c08fc0c93
df(5)

# ╔═╡ 98c2511d-c050-4c42-92f8-85d51d55f880
myloss(W, b, x) = sum(W * x .+ b)

# ╔═╡ 5de76bd7-707f-45a9-84a5-8dc5bc99e516
m = Dense(10, 5)

# ╔═╡ 120432f3-397c-4ea1-9b14-e3536c6da640
function make_batch(X, Y, idxs)
    Xbatch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        Xbatch[:, :, :, i] = Float32.(X[idxs[i]])
    end
	
    Ybatch = onehotbatch(Y[idxs], 0:9)
    return (Xbatch, Ybatch)
end

# ╔═╡ 8d8d2a43-ec5b-43af-9fd5-475c31c100d1
Array{Tuple{Array{Float32,4},Flux.OneHotMatrix{Array{Flux.OneHotVector,1}}},1}

# ╔═╡ 4b261348-df95-43d0-a990-07598aa2b38a
model = Chain(
    # First convolution
    Conv((3, 3), 1=>16, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Second convolution
    Conv((3, 3), 16=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Third convolution
    Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
	
	Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
	
	Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    x -> reshape(x, :, size(x, 4)),
    Dense(288, 10),

    softmax,
)

# ╔═╡ 7e62d40f-b1e7-44f6-96ab-04db57b17047
size(train_set[1][1]) # training data Float32

# ╔═╡ b4f03f16-6f72-43dd-9804-7104c0c8ab55
size(train_set[1][2]) # OneHotVector labels

# ╔═╡ 0e14ee0a-bb8e-4551-aec2-7db5bcc11c3b
train_set[1][2][:,1]

# ╔═╡ dc0e4f9a-4383-4479-91f5-4fc64cfdbc0b
model(train_set[1][1])

# ╔═╡ 560bc930-4b67-437a-8823-0c3b012c7920
function loss(x, y)
    x_aug = x .+ 0.1f0*gpu(randn(eltype(x), size(x)))

    y_hat = model(x_aug)
    return crossentropy(y_hat, y)
end

# ╔═╡ ecbaf6c3-9fb0-47d1-aa42-5bc78f34fb9b
accuracy(x, y) = mean(onecold(model(x)) .== onecold(y))

# ╔═╡ 04fa23e6-a10d-4c64-9102-5978384dc229
opt = ADAM(0.001)

# ╔═╡ a8961f8e-cf13-4494-a62c-70d9da80d67d
begin
	augment(x) = x .+ gpu(0.1f0*randn(eltype(x), size(x)))
	
	# Returns a vector of all parameters used in model
    paramvec(m) = hcat(map(p->reshape(p, :), params(m))...)

    # Function to check if any element is NaN or not
    anynan(x) = any(isnan.(x))

    accuracy(x, y, model) = mean(onecold(cpu(model(x))) .== onecold(cpu(y)))
end

# ╔═╡ e0e87368-e2a2-404e-a286-5eb6f4d0dc6a
begin
	best_acc = 0.0
    last_improvement = 0
    for epoch_idx in 1:100
		global best_acc, last_improvement
        # Train for a single epoch
        Flux.train!(loss, params(model), train_set, opt)

        # Calculate accuracy:
        acc = accuracy(test_set...)
    
        # If our accuracy is good enough, quit out.
        if acc >= 0.999
     
      end

       # If this is the best accuracy we've seen so far, save the model out
        if acc >= best_acc
        @info(" -> New best accuracy! Saving model out to mnist_conv.bson")
        BSON.@save "mnist_conv.bson" model epoch_idx acc
        best_acc = acc
        last_improvement = epoch_idx
        end

        if epoch_idx - last_improvement >= 5 && opt.eta > 1e-6
			opt.eta /= 10.0
     
        last_improvement = epoch_idx
        end

        if epoch_idx - last_improvement >= 10
        
		end
    end
end

# ╔═╡ adee6498-87b5-4121-8b3b-62abf837bc18
#function loss(x, y)
#	x̂ = augment(x)
 #   ŷ = model(x̂)
 #   return logitcrossentropy(ŷ, y)
#end

# ╔═╡ b37de186-3e6f-48ab-88b2-58a3e3cb3b4c
#lr = 0.1

# ╔═╡ ac9eb872-23b3-4264-a65a-2bc4ac6feccd
function sigmoid(z)
1 ./ (1 .+ exp.(.-z))
end

# ╔═╡ 44d4f348-032c-4cb4-9972-3a09f2907fbc
function transform_features(xMatrix, u, o)
       XNorm = (xMatrix .- u) ./ o
       return X_norm
       end

# ╔═╡ e5f4aa4f-5155-46f4-880e-7e6c851da656
begin
	W = randn(3, 5)
    b = zeros(3)
    x = rand(5)
end

# ╔═╡ 9706f75f-e698-486a-aa41-b0a8a6f9e391
gradient(myloss, W, b, x)

# ╔═╡ 6998e512-bd4d-4e9b-af63-7df9807ff5b6

begin
	test_set = make_minibatch(test_imgs, test_labels, 1:length(test_imgs))
	typeof(train_set)
end

# ╔═╡ 35230e38-8f49-4a93-97be-ed0076b0fe0a
begin
	train_set = gpu.(train_set)
    test_set = gpu.(test_set)
    modl = gpu(model)
end

# ╔═╡ 36c24522-4e8f-4b49-b23c-8f41378a6ea8
begin
	batch_size = 128
    mb_idxs = partition(1:length(train_imgs), batch_size)
    train_set = [make_minibatch(train_imgs, train_labels, i) for i in mb_idxs]
end

# ╔═╡ Cell order:
# ╠═ea4c8e60-b428-11ec-3423-81a7dd55b8b8
# ╠═49a97c78-8b58-4654-8aa8-a9779a7deaa2
# ╠═16d408e2-f184-403f-adc0-ca60591ec17a
# ╠═db8c5276-e81e-4e6a-8ce6-f0325f0405fd
# ╠═f856e09c-d9ff-4781-9e7e-315cba7a013a
# ╠═32a74df5-19e6-4946-9e21-ee7141b640a1
# ╠═2c4952bf-8af5-40ec-a0fe-dde8ecee7216
# ╠═a656c9a2-4072-459e-be9c-d20a519fad0a
# ╠═14338b42-65b3-4f65-a7f6-9b929a29bd94
# ╠═7af02932-45bd-4021-acc8-4b097dea8f4f
# ╠═550173b7-47d8-4a2c-8474-222f7c1e0323
# ╠═3dd6294e-2f50-45cf-bfa6-0d06c0cece91
# ╠═ede21768-43f2-4346-be4a-9b2bfa499177
# ╠═7efd48e1-338a-464c-9a2c-1018de7c37ef
# ╠═97ab55f2-3555-46e5-97e2-f63562c26374
# ╠═99824380-2798-4073-bd21-f21187af8a01
# ╠═b4f0f8d2-2f42-4199-a57f-a69219825428
# ╠═6e4087b1-df9a-474f-913b-9b2f44342fc9
# ╠═2cc81e1a-cd01-4d6b-8a5d-72f382b28c29
# ╠═3f605167-fa4b-45eb-a3cd-e9b8f322dcf0
# ╠═f1087c03-7dda-402a-a509-c821001c8b21
# ╠═a7e86e21-4e6d-4ee8-9af5-e392eb827312
# ╠═f45d729d-5361-403a-b134-e7b646de4722
# ╠═38a8168f-06bd-4b93-a151-078eb81358e0
# ╠═acc4158d-7df6-42fb-a930-8537eb2ac0bb
# ╠═907a88aa-adf9-487a-b3e4-2e5c08fc0c93
# ╠═98c2511d-c050-4c42-92f8-85d51d55f880
# ╠═5de76bd7-707f-45a9-84a5-8dc5bc99e516
# ╠═9706f75f-e698-486a-aa41-b0a8a6f9e391
# ╠═120432f3-397c-4ea1-9b14-e3536c6da640
# ╠═36c24522-4e8f-4b49-b23c-8f41378a6ea8
# ╠═6998e512-bd4d-4e9b-af63-7df9807ff5b6
# ╠═8d8d2a43-ec5b-43af-9fd5-475c31c100d1
# ╠═7e62d40f-b1e7-44f6-96ab-04db57b17047
# ╠═b4f03f16-6f72-43dd-9804-7104c0c8ab55
# ╠═0e14ee0a-bb8e-4551-aec2-7db5bcc11c3b
# ╠═4b261348-df95-43d0-a990-07598aa2b38a
# ╠═35230e38-8f49-4a93-97be-ed0076b0fe0a
# ╠═dc0e4f9a-4383-4479-91f5-4fc64cfdbc0b
# ╠═560bc930-4b67-437a-8823-0c3b012c7920
# ╠═ecbaf6c3-9fb0-47d1-aa42-5bc78f34fb9b
# ╠═04fa23e6-a10d-4c64-9102-5978384dc229
# ╠═e0e87368-e2a2-404e-a286-5eb6f4d0dc6a
# ╠═a8961f8e-cf13-4494-a62c-70d9da80d67d
# ╠═adee6498-87b5-4121-8b3b-62abf837bc18
# ╠═b37de186-3e6f-48ab-88b2-58a3e3cb3b4c
# ╠═ac9eb872-23b3-4264-a65a-2bc4ac6feccd
# ╠═44d4f348-032c-4cb4-9972-3a09f2907fbc
# ╠═e5f4aa4f-5155-46f4-880e-7e6c851da656
