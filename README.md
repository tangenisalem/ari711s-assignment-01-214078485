# ARI711S Assignment 01 - 214078485 

## Author
Full Name:  Salem Tangeni Hailonga<br>
Student No. 214078485<br>
Mode of Study: Part-Time<br>

## Introduction

After careful reading and grasping the assignment concept, It is required of me to give a solution to the below problems stipulated in the assignment using Julia programming language and use the Flux package for neural networks.

### Problem 1.
Consider the real estate dataset in https://www.kaggle.com/quantbruce/real-estate-price-prediction. Your task is to implement a linear regression
model using a multi-layer perceptron (MLP) and the Ridge regularisation in the Julia programming language. You will use the Flux package for the artificial neural network. 

### Problem 2.
In this problem, you will build an image classifier using a convolutional neural network (CNN). The images, accessible at https://www.kaggle.com/paultimothymooney/chest-xray-pneumonia, represent chest X-ray images of patients. Two possible types of labels are considered: normal and pneumonia.
Put another way, each sample represents either a normal patient or a patient suffering from pneumonia. You are required to build your model following AlexNet. Details about the AlexNet architecture can be found in "ImageNet Classification with Deep Convolutional Neural Networks" by Krizhevsky et al.


